CREATE TABLE tareas (id INT PRIMARY KEY AUTO_INCREMENT, codigoLista VARCHAR(255), item VARCHAR(255), descripcion VARCHAR(255), finalizado BOOLEAN);
INSERT INTO tareas (codigoLista, item, descripcion, finalizado) VALUES('Lista1', 'Item1', 'Recoger a los niños', false);
INSERT INTO tareas (codigoLista, item, descripcion, finalizado) VALUES('Lista1', 'Item2', 'Hacer la compra', false);
INSERT INTO tareas (codigoLista, item, descripcion, finalizado) VALUES('Lista1', 'Item3', 'Hacer el informe', false);

INSERT INTO tareas (codigoLista, item, descripcion, finalizado) VALUES('Lista2', 'Item1', 'Llevar coche al taller', false);
INSERT INTO tareas (codigoLista, item, descripcion, finalizado) VALUES('Lista2', 'Item2', 'Devolver paquete Amazon', false);
INSERT INTO tareas (codigoLista, item, descripcion, finalizado) VALUES('Lista2', 'Item3', 'Hacer entrevista', false);
INSERT INTO tareas (codigoLista, item, descripcion, finalizado) VALUES('Lista2', 'Item4', 'Llamar Juan', false);