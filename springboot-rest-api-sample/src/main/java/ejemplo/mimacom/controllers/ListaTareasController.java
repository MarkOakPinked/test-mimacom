package ejemplo.mimacom.controllers;

import javax.servlet.http.HttpServletResponse;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import ejemplo.mimacom.database.TareasRepository;
import ejemplo.mimacom.model.Lista;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 *
 * Ejemplo código API Rest back controller 
 */
@RestController
public class ListaTareasController {
	
	private Logger LOGGER = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private TareasRepository tareasRepository;
	
    /**
     *
     * @param id El identificador de la lista
     * @return Listas de tareas
     */
	@GetMapping(value = "/listas/{id}", produces = "application/json")
	@ApiOperation(value = "Lista de servicios")
    @ApiResponses({
        @ApiResponse(code = HttpServletResponse.SC_OK, message = "Operación correcta"),
        @ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, message = "Fallo en petición"),
        @ApiResponse(code = HttpServletResponse.SC_INTERNAL_SERVER_ERROR, message = "Error interno")
    })
    public ResponseEntity<Lista>  listado (@PathVariable("id") String codigoLista) {
		LOGGER.debug("listado");
		

		Lista lista = tareasRepository.findByTableName(codigoLista);
		return new ResponseEntity<>(lista, HttpStatus.OK);
    }
	
	
	
	/**
    *
    * @return true si ha ido ok 
    */
	@PostMapping(path = "listas/crear", consumes = "application/json", produces = "application/json")
	@ApiOperation(value = "Añade una lista")
    @ApiResponses({
        @ApiResponse(code = HttpServletResponse.SC_OK, message = "Operación correcta"),
        @ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, message = "Fallo en petición"),
        @ApiResponse(code = HttpServletResponse.SC_INTERNAL_SERVER_ERROR, message = "Error interno")
    })
    @ResponseStatus(HttpStatus.OK)
	public void creacion (@RequestBody  Lista request) {
		LOGGER.debug("creacion");

		tareasRepository.addTask(request);
	}
		

	
    /**
    *
    * @param nombrelista El nombre de la lista
    * @return true si ha ido ok  
    */
	@DeleteMapping(path = "listas/{id}/eliminar/{tarea}", produces = "application/json")
	@ApiOperation(value = "Elimina una tarea")
    @ApiResponses({
        @ApiResponse(code = HttpServletResponse.SC_OK, message = "Operación correcta"),
        @ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, message = "Fallo en petición"),
        @ApiResponse(code = HttpServletResponse.SC_INTERNAL_SERVER_ERROR, message = "Error interno")
    })
	public void borrado (@PathVariable("id") String codigoLista, @PathVariable("tarea") String tarea ) {
		LOGGER.debug("borrado");

		tareasRepository.removeTask(codigoLista, tarea);
	}
	
		
	
    /**
    *
    * @param nombrelista El identificador de la lista
    * @return true si ha ido ok  
    */
	@PutMapping (path = "listas/{id}/editar/{tarea}", produces = "application/json")
	@ApiOperation(value = "Editamos una lista")
    @ApiResponses({
        @ApiResponse(code = HttpServletResponse.SC_OK, message = "Operación correcta"),
        @ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, message = "Fallo en petición"),
        @ApiResponse(code = HttpServletResponse.SC_INTERNAL_SERVER_ERROR, message = "Error interno")
    })
    @ResponseStatus(HttpStatus.OK)
	public void edicion (@PathVariable("id") String codigoLista, @RequestBody  Lista request ) {
		LOGGER.debug("edicion");

		tareasRepository.editTask(codigoLista, request);
	}

	
	
    /**
    *
    * @param id El identificador de la lista
    * @param tarea La tarea a finalizar
    * @return true si ha ido ok  
    */
	@PutMapping (path = "listas/{id}/finalizar/{tarea}", produces = "application/json")
	@ApiOperation(value = "Finalizar una tarea dada")
    @ApiResponses({
        @ApiResponse(code = HttpServletResponse.SC_OK, message = "Operación correcta"),
        @ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, message = "Fallo en petición"),
        @ApiResponse(code = HttpServletResponse.SC_INTERNAL_SERVER_ERROR, message = "Error interno")
    })
    @ResponseStatus(HttpStatus.OK)
	public void finalizarTarea (@PathVariable("id") String codigoLista, @PathVariable("tarea") String tarea ) {
		LOGGER.debug("finalizado");

		tareasRepository.setTaskEnded(codigoLista, tarea);
	}	
	
	
}// De la clase
