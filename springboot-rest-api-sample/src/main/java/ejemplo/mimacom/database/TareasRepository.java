package ejemplo.mimacom.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Repository;

import ejemplo.mimacom.model.Item;
import ejemplo.mimacom.model.Lista;


@Repository
@PropertySource("classpath:application.properties")
public class TareasRepository {

	
	@Value("${spring.datasource.driverClassName}")
    private String driver;
	
	@Value("${spring.datasource.url}")
    private String connection;
	
	@Value("${spring.datasource.username}")
    private String user;
    
    @Value("${spring.datasource.password}")
    private String password;
	
	
    private static final String SELECT_BY_ID = "SELECT * FROM TAREAS WHERE CODIGOLISTA = ?";
    
    private static final String FINISH_TASK = "UPDATE TAREAS SET FINALIZADO = TRUE WHERE CODIGOLISTA = ? AND ITEM = ?";
	
    private static final String REMOVE_TASK = "DELETE FROM TAREAS WHERE CODIGOLISTA = ? AND ITEM = ?";
    
    private static final String ADD_TASK = "INSERT INTO TAREAS (codigoLista, item, descripcion, finalizado) VALUES (?, ?, ?, ?)";
    
    private static final String EDIT_TASK = "UPDATE TAREAS SET DESCRIPCION = ? , FINALIZADO = ? WHERE CODIGOLISTA = ? AND ITEM = ?";
    
    private Logger LOGGER = LoggerFactory.getLogger(this.getClass());


    public Lista findByTableName(String codigoLista) {
    	
    	Lista lista = new Lista();
    	ArrayList<Item> elementos = new ArrayList<>();

        try {
        	Connection con = DriverManager.getConnection(connection, user, password);
            PreparedStatement ps = con.prepareStatement(SELECT_BY_ID);
        	ps.setString(1, codigoLista); 
        	
            ResultSet rs = ps.executeQuery();

        	lista.setCodigoLista(codigoLista);
            while (rs.next()) {
            	Item item = new Item();
            	item.setItem(rs.getString("ITEM"));
            	item.setDescripcion(rs.getString("DESCRIPCION"));
            	item.setFinalizado(rs.getBoolean("FINALIZADO"));
            	elementos.add(item);
            }
            lista.setElementos(elementos);

        } catch (SQLException ex) {

        	LOGGER.error (ex.getMessage(), ex);
        }

        return lista;
    }

    public boolean setTaskEnded(String codigoLista, String item) {
    	
    	boolean result_ok = false;

        try {
        	Connection con = DriverManager.getConnection(connection, user, password);
            PreparedStatement ps = con.prepareStatement(FINISH_TASK);
        	ps.setString(1, codigoLista);
        	ps.setString(2, item);
        	
            int result = ps.executeUpdate();
            
            result_ok = result>0;

        } catch (SQLException ex) {

        	LOGGER.error (ex.getMessage(), ex);
        }

        return result_ok;
    }

    public boolean removeTask(String codigoLista, String item) {
    	
    	boolean result_ok = false;

        try {
        	Connection con = DriverManager.getConnection(connection, user, password);
            PreparedStatement ps = con.prepareStatement(REMOVE_TASK);
        	ps.setString(1, codigoLista);
        	ps.setString(2, item);
        	
            int result = ps.executeUpdate();
            
            result_ok = result>0;

        } catch (SQLException ex) {

        	LOGGER.error (ex.getMessage(), ex);
        }

        return result_ok;
    }
    
    public boolean editTask(String codigoLista, Lista lista) {
    	
    	boolean result_ok = false;
    	int result = 0;
        try {
        	Connection con = DriverManager.getConnection(connection, user, password);
            PreparedStatement ps = con.prepareStatement(EDIT_TASK); 
            
            for (Item item: lista.getElementos()) {
            	ps.setString(1, item.getDescripcion());
            	ps.setBoolean(2, item.isFinalizado());
            	ps.setString(3, codigoLista);
            	ps.setString(4, item.getItem());
            	result = ps.executeUpdate();
            }

            result_ok = result>0;

        } catch (SQLException ex) {

        	LOGGER.error (ex.getMessage(), ex);
        }

        return result_ok;
    }
    
    public boolean addTask(Lista lista) {
    	
    	boolean result_ok = false;
    	int result = 0;

        try {
        	Connection con = DriverManager.getConnection(connection, user, password);
            PreparedStatement ps = con.prepareStatement(ADD_TASK); 
            
            for (Item item: lista.getElementos()) {
            	ps.setString(1, lista.getCodigoLista());
            	ps.setString(2, item.getItem());
            	ps.setString(3, item.getDescripcion());
            	ps.setBoolean(4, item.isFinalizado());
            	result = ps.executeUpdate();
            }

            result_ok = result>0;

        } catch (SQLException ex) {

        	LOGGER.error (ex.getMessage(), ex);
        }

        return result_ok;
    }
    
} // De la clase
