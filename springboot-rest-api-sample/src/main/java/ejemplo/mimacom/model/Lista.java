package ejemplo.mimacom.model;

import java.util.List;

public class Lista {
	
	private String codigoLista;

	private List<Item> elementos;

	public String getCodigoLista() {
		return codigoLista;
	}

	public void setCodigoLista(String codigoLista) {
		this.codigoLista = codigoLista;
	}

	public List<Item> getElementos() {
		return elementos;
	}

	public void setElementos(List<Item> elementos) {
		this.elementos = elementos;
	}
	
}// De la clase
