package ejemplo.mimacom.requests;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import ejemplo.mimacom.database.TareasRepository;
import ejemplo.mimacom.model.Item;
import ejemplo.mimacom.model.Lista;



@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class  ListaTareasControllerTest {

	
	@Autowired
	private MockMvc mockMvc;

	
	@Autowired
	private WebApplicationContext webApplicationContext;
	
	@Mock
	private TareasRepository tareasRepository;
	
	@Before
	  public void setUp() {
	    mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
	  }
	
	@Before
	public void  init() {
		
		Item item = new Item();
		item.setItem("ItemTest");
		item.setDescripcion("Descripcion Item test");
		item.setFinalizado(false);
		
		List<Item> elementos = new ArrayList<Item>();
		elementos.add(item);
		
		Lista lista = new Lista();
		lista.setCodigoLista("Lista Test");
		lista.setElementos(elementos);
		
		Mockito.when(tareasRepository.findByTableName(Mockito.any())).thenReturn(lista);
		Mockito.when(tareasRepository.removeTask(Mockito.any(), Mockito.any())).thenReturn(true);
		Mockito.when(tareasRepository.setTaskEnded(Mockito.any(), Mockito.any())).thenReturn(true);
	}
	
	
	@Test
	public void findByTableNameTest() throws Exception {
		this.mockMvc.perform(get("/listas/Lista1")).andExpect(status().isOk())
			.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));

	}
	
	@Test
	public void setTaskEndedTest() throws Exception {
		this.mockMvc.perform(put("/listas/1/finalizar/1")).andExpect(status().isOk());
	}
	
	@Test
	public void removeTaskTest() throws Exception {
		this.mockMvc.perform(delete("/listas/1/eliminar/1")).andExpect(status().isOk());
	}

} // De la clase
