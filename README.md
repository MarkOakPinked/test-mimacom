# Test mimacom

Para poner en marcha el proyecto hacer 

mvn install

y a continuación

mvn spring-boot:run

Una vez arrancado se puede consultar el API en:
http://localhost:8000/swagger-ui.html#/

Y para las consultas en la BBDD:
http://localhost:8000/h2-console/login.jsp

(no necesita password)